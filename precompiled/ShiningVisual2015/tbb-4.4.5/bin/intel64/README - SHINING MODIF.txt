# note: vc12 est en réalité le contenu de vc14 dans l'archive de TBB compilé pour Windows.
Pour une raison inconnue, le CMake de embree essaye de récupérer les dll du dossier vc12 plutot que vc14
(le problème n'apparait pas pour vc11).