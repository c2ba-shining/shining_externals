First download boost_1_57_0.7z at https://sourceforge.net/projects/boost/files/boost/1.57.0/ 
and put it in this directory.

Then run build-all.sh in a bash console to download and compile all shining dependencies.

Archive to add to shining will be put in shining-externals/windows