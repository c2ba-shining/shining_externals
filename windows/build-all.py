#!/bin/python3

import os
import shutil
from contextlib import contextmanager
import sys
import locale
import urllib2
import argparse

LLVM_VERSION="5.0.2"
LLVM_SUFFIX = LLVM_VERSION.replace(".", "_")

OIIO_VERSION = "1.8.14"
OIIO_SUFFIX = OIIO_VERSION.replace(".", "_")

OSL_VERSION = "1.9.10"
OSL_SUFFIX = OSL_VERSION.replace(".", "_")

EMBREE_VERSION = "2.12.0"
EMBREE_SUFFIX = EMBREE_VERSION.replace(".", "_")

OPENSUBDIV_VERSION = "3_0_0"
OPENSUBDIV_SUFFIX = "3_0_0"

BUILD_TYPES = [ "Release", "Debug" ]
#BUILD_TYPES = [ "Release" ]

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))

PATH_7ZIP = os.path.join(SCRIPT_DIR, "7za.exe")
PATH_VSBUILD = os.path.join(SCRIPT_DIR, "build-vs.bat")
WIN_FLEX_BISON = os.path.join(SCRIPT_DIR, "win_flex_bison")

ROOT_DIR = os.getcwd().replace("\\", "/")
CMAKE_INSTALL_PREFIX = [ ROOT_DIR +  "/dist-" + build_type for build_type in BUILD_TYPES ]
SHINING_LIB_DIR = ROOT_DIR + "/shining-externals"

VS_VERSIONS = { "2015": "14" }
SHINING_3RD_PARTY_DIR = ""
DEFAULT_VS_VERSION = "2015"

# Global variables set in main from command arguments or default values:
VS_YEAR = ""
VS_VERSION = ""
BUILD_DIR_SUFFIX = []
CMAKE_GENERATOR = ""

def mkdir(path):
    if not os.path.exists(path):
        os.makedirs(path)

def copy(src, dst):
    if os.path.isdir(src):
        if os.path.exists(dst):
            copyfiles(src, dst)
        else:
            print("cp " + src + " -> " + dst)
            shutil.copytree(src, dst)
    else:
        print("cp " + src + " -> " + dst)
        shutil.copy(src, dst)

def copyfiles(dir, dstDir):
    for entry in os.listdir(dir):
        copy(os.path.join(dir, entry), os.path.join(dstDir, entry))

def git(command):
    os.system("git " + command)

def call7zip(command):
    os.system(PATH_7ZIP + " " + command)

def cmake(command):
    os.system('cmake -G "' + CMAKE_GENERATOR + '" -Thost=x64 ' + command)

def build(file, config):
    os.system(PATH_VSBUILD + " " + file + " " + config + " " + VS_VERSION)

def safeUrlOpen(url):
    try:
        return urllib2.urlopen(url)
    except urllib2.HTTPError as e:
        print ("HTTP Error: ", e.code, url)
        exit()
    except urllib2.URLError as e:
        print ("URLError: ", e.reason, url)
        exit()

@contextmanager
def pushd(newDir):
    previousDir = os.getcwd()
    os.chdir(newDir)
    yield
    os.chdir(previousDir)

def valid_list(str):
     value = str.split(",")
     return value

def build_libjpeg():
    PRECOMPILED_DIR = "jpeg-6b"
    for install_prefix in CMAKE_INSTALL_PREFIX:
        copyfiles(SHINING_3RD_PARTY_DIR + "/" + PRECOMPILED_DIR + "/lib/x64/Release", os.path.join(install_prefix, "lib"))
        copyfiles(SHINING_3RD_PARTY_DIR + "/" + PRECOMPILED_DIR + "/include", os.path.join(install_prefix, "include"))

def build_libtiff():
    PRECOMPILED_DIR = "tiff-4.0.3"
    for install_prefix in CMAKE_INSTALL_PREFIX:
        copyfiles(SHINING_3RD_PARTY_DIR + "/" + PRECOMPILED_DIR + "/lib/x64/Release", os.path.join(install_prefix, "lib"))
        copyfiles(SHINING_3RD_PARTY_DIR + "/" + PRECOMPILED_DIR + "/include", os.path.join(install_prefix, "include"))

def build_libpng():
    PRECOMPILED_DIR = "libpng-1.6.8"
    for install_prefix in CMAKE_INSTALL_PREFIX:
        copyfiles(SHINING_3RD_PARTY_DIR + "/" + PRECOMPILED_DIR + "/bin/x64/Release", os.path.join(install_prefix, "bin"))
        copyfiles(SHINING_3RD_PARTY_DIR + "/" + PRECOMPILED_DIR + "/lib/x64/Release", os.path.join(install_prefix, "lib"))
        copyfiles(SHINING_3RD_PARTY_DIR + "/" + PRECOMPILED_DIR + "/include", os.path.join(install_prefix, "include"))
    
    with pushd(SHINING_3RD_PARTY_DIR):
        call7zip("a " + SHINING_LIB_DIR + "/" + PRECOMPILED_DIR + ".7z " + PRECOMPILED_DIR)
    
    for install_prefix in CMAKE_INSTALL_PREFIX:
        # Trick for Find(PNG) to work during oiio compilation
        png_lib_trick_name = os.path.join(install_prefix, "lib", "png.lib")
        if os.path.isfile(png_lib_trick_name):
            os.remove(png_lib_trick_name)
        os.rename(os.path.join(install_prefix, "lib", "libpng16.lib"), png_lib_trick_name)

def build_zlib():
    PRECOMPILED_DIR = "zlib-1.2.8"
    for install_prefix in CMAKE_INSTALL_PREFIX:
        copyfiles(SHINING_3RD_PARTY_DIR + "/" + PRECOMPILED_DIR + "/x64/Release", os.path.join(install_prefix, "lib"))
        copy(SHINING_3RD_PARTY_DIR + "/" + PRECOMPILED_DIR + "/zconf.h", os.path.join(install_prefix, "include", "zconf.h"))
        copy(SHINING_3RD_PARTY_DIR + "/" + PRECOMPILED_DIR + "/zlib.h", os.path.join(install_prefix, "include", "zlib.h"))

def build_openexr():
    LIB_NAME="OpenEXR_2_2"
    GIT_TAG="v2.2.0"
    CLONE_DIR="repo-" + LIB_NAME

    if not os.path.exists(CLONE_DIR):
        git("clone https://github.com/openexr/openexr " + CLONE_DIR)
        with pushd(CLONE_DIR):
            git("checkout " + GIT_TAG)

    for LIB_NAME in [ "Ilmbase_2_2", "OpenEXR_2_2" ]:
        BUILD_DIR = [ ROOT_DIR + "/build-" + LIB_NAME + x for x in BUILD_DIR_SUFFIX ]

        for i, build_type in enumerate(BUILD_TYPES):
            if not os.path.exists(BUILD_DIR[i]):
                mkdir(BUILD_DIR[i])
                with pushd(BUILD_DIR[i]):
                    if LIB_NAME == "Ilmbase_2_2":
                        cmake(" -DCMAKE_BUILD_TYPE=" + build_type +
                            " -DCMAKE_INSTALL_PREFIX=" + CMAKE_INSTALL_PREFIX[i] +
                            " -DCMAKE_PREFIX_PATH=" + CMAKE_INSTALL_PREFIX[i] +
                            " -DCMAKE_CXX_FLAGS_RELEASE=/Zi" +
                            " -DCMAKE_SHARED_LINKER_FLAGS_RELEASE=/DEBUG /OPT:REF /OPT:ICF" +
                            " " + ROOT_DIR + "/" + CLONE_DIR + "/IlmBase")
                    else:
                        cmake(" -DCMAKE_BUILD_TYPE=" + build_type + 
                            " -DCMAKE_INSTALL_PREFIX=" + CMAKE_INSTALL_PREFIX[i] +
                            " -DCMAKE_PREFIX_PATH=" + CMAKE_INSTALL_PREFIX[i] +
                            " -DILMBASE_PACKAGE_PREFIX=" + CMAKE_INSTALL_PREFIX[i] + 
                            " -DZLIB_ROOT=" + CMAKE_INSTALL_PREFIX[i] +
                            " -DCMAKE_CXX_FLAGS_RELEASE=/Zi" +
                            " -DCMAKE_SHARED_LINKER_FLAGS_RELEASE=/DEBUG /OPT:REF /OPT:ICF" +
                            " " + ROOT_DIR + "/" + CLONE_DIR + "/OpenEXR")
                        mkdir("IlmImf/"+build_type)
                        copy(CMAKE_INSTALL_PREFIX[i]+"/lib/Half.dll", "IlmImf/"+build_type+"/Half.dll")
                        copy(CMAKE_INSTALL_PREFIX[i]+"/lib/Iex-2_2.dll", "IlmImf/"+build_type+"/Iex-2_2.dll")
                        copy(CMAKE_INSTALL_PREFIX[i]+"/lib/IlmThread-2_2.dll", "IlmImf/"+build_type+"/IlmThread-2_2.dll")
            with pushd(BUILD_DIR[i]):
                build("INSTALL.vcxproj", build_type)

                if LIB_NAME == "Ilmbase_2_2":
                    copy(BUILD_DIR[i] + "/Half/" + build_type + "/Half.pdb", CMAKE_INSTALL_PREFIX[i] + "/lib/Half.pdb")
                    copy(BUILD_DIR[i] + "/Iex/" + build_type + "/Iex-2_2.pdb", CMAKE_INSTALL_PREFIX[i] + "/lib/Iex-2_2.pdb")
                    copy(BUILD_DIR[i] + "/IlmThread/" + build_type + "/IlmThread-2_2.pdb", CMAKE_INSTALL_PREFIX[i] + "/lib/IlmThread-2_2.pdb")
                    copy(BUILD_DIR[i] + "/Imath/" + build_type + "/Imath-2_2.pdb", CMAKE_INSTALL_PREFIX[i] + "/lib/Imath-2_2.pdb")
                else:
                    copy(BUILD_DIR[i] + "/IlmImf/" + build_type + "/IlmImf-2_2.pdb", CMAKE_INSTALL_PREFIX[i] + "/lib/IlmImf-2_2.pdb")

def build_llvm():
    LLVM_AR_EXT = ".tar.xz"
    LLVM_DIR="llvm-" + LLVM_VERSION + ".src"
    LLVM_AR_FILE = LLVM_DIR + LLVM_AR_EXT

    if not os.path.exists(LLVM_DIR):
        if not os.path.exists(LLVM_AR_FILE):
            print ("Downloading " + LLVM_AR_FILE)
            f = safeUrlOpen("http://releases.llvm.org/" + LLVM_VERSION + "/" + LLVM_AR_FILE)
            with open(LLVM_AR_FILE, "wb") as localFile:
                localFile.write(f.read())
            f.close()
        call7zip("x " + LLVM_AR_FILE)
        call7zip("x " + LLVM_DIR + ".tar")
        os.remove(LLVM_DIR + ".tar")

    LIB_NAME="llvm_" + LLVM_SUFFIX
    BUILD_DIR = [ ROOT_DIR + "/build-" + LIB_NAME + x for x in BUILD_DIR_SUFFIX ]

    for i, build_type in enumerate(BUILD_TYPES):
        if not os.path.exists(BUILD_DIR[i]):
            mkdir(BUILD_DIR[i])
            with pushd(BUILD_DIR[i]):
                cmake(" -DCMAKE_BUILD_TYPE=" + build_type +
                    " -DCMAKE_INSTALL_PREFIX=" + CMAKE_INSTALL_PREFIX[i] +
                    " -DCMAKE_PREFIX_PATH=" + CMAKE_INSTALL_PREFIX[i] +
                    " ../" + LLVM_DIR)
        with pushd(BUILD_DIR[i]):
            build("INSTALL.vcxproj", build_type)

def build_boost():
    LIB_NAME="boost_1_57_0"
    INSTALL_PREFIX = [ os.path.join(ROOT_DIR, "dist-" + LIB_NAME + "-" + build_type) for build_type in BUILD_TYPES ]

    if not os.path.exists(LIB_NAME):
        if not os.path.exists(LIB_NAME + ".7z"):
            assert(os.path.exists(SHINING_3RD_PARTY_DIR + "/" + LIB_NAME + ".7z"))
            copy(SHINING_3RD_PARTY_DIR + "/" + LIB_NAME + ".7z", LIB_NAME + ".7z")
        
        call7zip("x " + LIB_NAME + ".7z")
        with pushd(LIB_NAME):
            os.system("bootstrap.bat")
    
    with pushd(LIB_NAME):
        for i, build_type in enumerate(BUILD_TYPES): 
            if not os.path.exists(INSTALL_PREFIX[i]):
                os.system("b2 headers")
                os.system("b2 --prefix=" + INSTALL_PREFIX[i] + " --toolset=msvc-" + VS_VERSION + ".0 link=shared variant=" + build_type.lower() + " threading=multi address-model=64 -j8 install")
                shutil.move(INSTALL_PREFIX[i] + "/include/boost-1_57/boost", INSTALL_PREFIX[i] + "/include/boost")
                shutil.rmtree(INSTALL_PREFIX[i] + "/include/boost-1_57")

    if os.path.exists(SHINING_LIB_DIR + "/" + LIB_NAME + ".7z"):
        return

    print("Build boost archive " + SHINING_LIB_DIR + "/" + LIB_NAME + ".7z for shining...")

    mkdir(SHINING_LIB_DIR + "/" + LIB_NAME + "/include")
    mkdir(SHINING_LIB_DIR + "/" + LIB_NAME + "/lib")

    copyfiles(INSTALL_PREFIX[0] + "/include", SHINING_LIB_DIR + "/" + LIB_NAME + "/include")

    for i, build_type in enumerate(BUILD_TYPES):
        copyfiles(INSTALL_PREFIX[i] + "/include", CMAKE_INSTALL_PREFIX[i] + "/include")

    lib_basenames = [ "boost_chrono", "boost_date_time", "boost_filesystem", "boost_regex", "boost_system", "boost_thread", "boost_timer", "boost_wave" ]
    split_str = "-vc" + VS_VERSION + "0"

    for i, build_type in enumerate(BUILD_TYPES):
        for f in os.listdir(INSTALL_PREFIX[i] + "/lib"):
            tmp = f.split(split_str)
            if tmp[0] in lib_basenames:
                copy(INSTALL_PREFIX[i] + "/lib/" + f, SHINING_LIB_DIR + "/" + LIB_NAME + "/lib/" + f)
                copy(INSTALL_PREFIX[i] + "/lib/" + f, CMAKE_INSTALL_PREFIX[i] + "/lib/" + f)
    
    call7zip("a " + SHINING_LIB_DIR + "/" + LIB_NAME + ".7z " + SHINING_LIB_DIR + "/" + LIB_NAME)

def build_oiio():
    LIB_NAME="OpenImageIO_" + OIIO_SUFFIX
    BUILD_DIR = [ ROOT_DIR + "/build-" + LIB_NAME + x for x in BUILD_DIR_SUFFIX ]
    GIT_TAG="Release-" + OIIO_VERSION
    CLONE_DIR="repo-" + LIB_NAME

    if not os.path.exists(CLONE_DIR):
        git("clone https://github.com/OpenImageIO/oiio " + CLONE_DIR)
        with pushd(CLONE_DIR):
            git("checkout " + GIT_TAG)

    for i, build_type in enumerate(BUILD_TYPES):
        cmake_build_type = "Debug" if build_type == "Debug" else "RelWithDebInfo"
        if not os.path.exists(BUILD_DIR[i]):
            mkdir(BUILD_DIR[i])
            with pushd(BUILD_DIR[i]):
                cmake(" -DCMAKE_BUILD_TYPE=" + cmake_build_type +
                    " -DCMAKE_INSTALL_PREFIX=" + CMAKE_INSTALL_PREFIX[i] +
                    " -DCMAKE_PREFIX_PATH=" + CMAKE_INSTALL_PREFIX[i] +
                    " -DVERBOSE=1" +
                    " -DUSE_SIMD=sse4.2" +
                    " -DCMAKE_LIBRARY_PATH=" + CMAKE_INSTALL_PREFIX[i] + "/lib"
                    " -DOPENEXR_IEX_LIBRARY=" + CMAKE_INSTALL_PREFIX[i] + "/lib/Iex-2_2.lib"
                    " -DOPENEXR_ILMIMF_LIBRARY=" + CMAKE_INSTALL_PREFIX[i] + "/lib/IlmImf-2_2.lib"
                    " -DOPENEXR_ILMTHREAD_LIBRARY=" + CMAKE_INSTALL_PREFIX[i] + "/lib/IlmThread-2_2.lib"
                    " -DOPENEXR_IMATH_LIBRARY=" + CMAKE_INSTALL_PREFIX[i] + "/lib/Imath-2_2.lib"
                    " -DILMBASE_HOME=" + CMAKE_INSTALL_PREFIX[i] +
                    " -DOPENEXR_HOME=" + CMAKE_INSTALL_PREFIX[i] +
                    " -DBOOST_ROOT=" + CMAKE_INSTALL_PREFIX[i] +
                    " -DUSE_PTEX=0"
                    " -DUSE_FIELD3D=0"
                    " -DUSE_PYTHON=0"
                    " -DUSE_QT=0"
                    " -DUSE_NUKE=0"
                    " -DUSE_FFMPEG=0"
                    " -DUSE_GIF=0"
                    " -DUSE_FREETYPE=0"
                    " -DUSE_OCIO=0"
                    " -DUSE_OPENCV=0"
                    " -DUSE_OPENJPEG=0"
                    " -DUSE_OPENSSL=0"
                    " -DUSE_OPENGL=0"
                    " -DUSE_JPEGTURBO=0"
                    " -DUSE_LIBRAW=0"
                    " -DBUILD_TESTING=0"
                    " -DOIIO_BUILD_TOOLS=0"
                    " -DOIIO_BUILD_TESTS=0"
                    " -DINSTALL_DOCS=0"
                    " -DINSTALL_FONTS=0"
                    " -DTIFF_INCLUDE_DIR=" + CMAKE_INSTALL_PREFIX[i] + "/include"
                    " -DTIFF_LIBRARY=" + CMAKE_INSTALL_PREFIX[i] + "/lib/libtiff.lib"
                    " -DJPEG_LIBRARY=" + CMAKE_INSTALL_PREFIX[i] + "/lib/libjpeg.lib"
                    " -DJPEG_INCLUDE_DIR=" + CMAKE_INSTALL_PREFIX[i] + "/include"
                    " -DPNG_INCLUDE_DIR=" + CMAKE_INSTALL_PREFIX[i] + "/include"
                    " -DPNG_LIBRARIES=" + CMAKE_INSTALL_PREFIX[i] + "/lib/libpng16.lib"
                    " -DZLIB_LIBRARY_RELEASE=" + CMAKE_INSTALL_PREFIX[i] + "/lib/zlib.lib"
                    " -DZLIB_INCLUDE_DIR=" + CMAKE_INSTALL_PREFIX[i] + "/include"
                    " -DCMAKE_CXX_FLAGS_RELEASE=/Zi" +
                    " -DCMAKE_SHARED_LINKER_FLAGS_RELEASE=/DEBUG /OPT:REF /OPT:ICF" +
                    " " + ROOT_DIR + "/" + CLONE_DIR)

        with pushd(BUILD_DIR[i]):
            build("INSTALL.vcxproj", cmake_build_type)

    OUTPUT = SHINING_LIB_DIR + "/" + LIB_NAME
    if os.path.exists(OUTPUT + ".7z"):
        return
    
    print("Build OpenImageIO archive " + OUTPUT + ".7z for shining...")

    mkdir(OUTPUT + "/include")
    copy(CMAKE_INSTALL_PREFIX[0] + "/include/OpenEXR", OUTPUT + "/include/OpenEXR")
    copy(CMAKE_INSTALL_PREFIX[0] + "/include/OpenImageIO", OUTPUT + "/include/OpenImageIO")

    for i, build_type in enumerate(BUILD_TYPES):
        cmake_build_type = "Debug" if build_type == "Debug" else "RelWithDebInfo"
        mkdir(OUTPUT + "/lib/" + build_type)
        mkdir(OUTPUT + "/bin/" + build_type)
        lib_files = ["Iex-2_2.lib", "Imath-2_2.lib", "OpenImageIO.lib", "IlmImf-2_2.lib",
            "Half.dll", "Iex-2_2.dll", "IlmImf-2_2.dll", "IlmThread-2_2.dll", "Imath-2_2.dll", "zlib1.dll",
            "Half.pdb", "Iex-2_2.pdb", "IlmImf-2_2.pdb", "IlmThread-2_2.pdb", "Imath-2_2.pdb"]
        for f in lib_files:
            if f.endswith(".lib"):
                copy(CMAKE_INSTALL_PREFIX[i] + "/lib/" + f, OUTPUT + "/lib/" + build_type + "/" + f)
            else:
                copy(CMAKE_INSTALL_PREFIX[i] + "/lib/" + f, OUTPUT + "/bin/" + build_type + "/" + f)
        bin_files = ["libpng16.dll", "OpenImageIO.dll"]
        for f in bin_files:
            copy(CMAKE_INSTALL_PREFIX[i] + "/bin/" + f, OUTPUT + "/bin/" + build_type + "/" + f)
        copy(BUILD_DIR[i] + "/src/libOpenImageIO/" + cmake_build_type + "/OpenImageIO.pdb", OUTPUT + "/bin/" + build_type + "/OpenImageIO.pdb")
    
    call7zip("a " + OUTPUT + ".7z " + OUTPUT)

def build_OSL():
    LIB_NAME="OSL_" + OSL_SUFFIX
    BUILD_DIR = [ ROOT_DIR + "/build-" + LIB_NAME + x for x in BUILD_DIR_SUFFIX ]
    GIT_TAG = "Release-" + OSL_VERSION
    CLONE_DIR = "repo-" + LIB_NAME

    if not os.path.exists(CLONE_DIR):
        git("clone https://github.com/imageworks/OpenShadingLanguage " + CLONE_DIR)
        with pushd(CLONE_DIR):
            git("checkout " + GIT_TAG)
            '''
            git("rebase --abort") # Abort current rebase if necessary (Release-1.7.3)
            if os.path.exists(SCRIPT_DIR + "/OSL_shining_" + OSL_SUFFIX + ".patch"):
                git("am --signoff < " + SCRIPT_DIR + "/OSL_shining_" + OSL_SUFFIX + ".patch") # Apply patch for the version
            '''
    
    for i, build_type in enumerate(BUILD_TYPES):
        cmake_build_type = "Debug" if build_type == "Debug" else "RelWithDebInfo"
        if not os.path.exists(BUILD_DIR[i]):
            mkdir(BUILD_DIR[i])
            with pushd(BUILD_DIR[i]):
                cmake(" -DCMAKE_BUILD_TYPE=" + cmake_build_type +
                    " -DCMAKE_INSTALL_PREFIX=" + CMAKE_INSTALL_PREFIX[i] +
                    " -DCMAKE_PREFIX_PATH=" + CMAKE_INSTALL_PREFIX[i] +
                    " -DVERBOSE=1" +
                    " -DUSE_SIMD=sse4.2" +
                    " -DOPENEXR_IEX_LIBRARY=" + CMAKE_INSTALL_PREFIX[i] + "/lib/Iex-2_2.lib"
                    " -DOPENEXR_ILMIMF_LIBRARY=" + CMAKE_INSTALL_PREFIX[i] + "/lib/IlmImf-2_2.lib"
                    " -DOPENEXR_ILMTHREAD_LIBRARY=" + CMAKE_INSTALL_PREFIX[i] + "/lib/IlmThread-2_2.lib"
                    " -DOPENEXR_IMATH_LIBRARY=" + CMAKE_INSTALL_PREFIX[i] + "/lib/Imath-2_2.lib"
                    " -DILMBASE_HOME=" + CMAKE_INSTALL_PREFIX[i] +
                    " -DOPENEXR_HOME=" + CMAKE_INSTALL_PREFIX[i] +
                    " -DBOOST_ROOT=" + CMAKE_INSTALL_PREFIX[i] +
                    " -DOSL_BUILD_TESTS=0" +
                    " -DLLVM_DIRECTORY=" + CMAKE_INSTALL_PREFIX[i] +
                    " -DLLVM_STATIC=1" +
                    " -DUSE_LLVM_BITCODE=0" +
                    " -DBISON_EXECUTABLE=" + WIN_FLEX_BISON + "/win_bison.exe" +
                    " -DFLEX_EXECUTABLE=" + WIN_FLEX_BISON + "/win_flex.exe" +
                    " -DCMAKE_INCLUDE_PATH=" + WIN_FLEX_BISON +
                    " -DCMAKE_CXX_FLAGS_RELEASE=/Zi" +
                    " -DCMAKE_SHARED_LINKER_FLAGS_RELEASE=/DEBUG /OPT:REF /OPT:ICF" +
                    " " + ROOT_DIR + "/" + CLONE_DIR)
                build("src/liboslcomp/oslcomp.vcxproj", cmake_build_type)
                build("src/oslc/oslc.vcxproj", cmake_build_type)
                copy(CMAKE_INSTALL_PREFIX[i] + "/bin/OpenImageIO.dll", os.getcwd() + "/src/oslc/" + cmake_build_type + "/OpenImageIO.dll")
                copy(os.getcwd() + "/src/liboslcomp/" + cmake_build_type + "/oslcomp.dll", os.getcwd() + "/src/oslc/" + cmake_build_type + "/oslcomp.dll")
                copy(CMAKE_INSTALL_PREFIX[i] + "/bin/libpng16.dll", os.getcwd() + "/src/oslc/" + cmake_build_type + "/libpng16.dll")
                for f in os.listdir(CMAKE_INSTALL_PREFIX[i] + "/lib/"):
                    if f.endswith(".dll"):
                        copy(CMAKE_INSTALL_PREFIX[i] + "/lib/" + f, os.getcwd() + "/src/oslc/" + cmake_build_type + "/" + f)
        with pushd(BUILD_DIR[i]):
            build("INSTALL.vcxproj", cmake_build_type)

    OUTPUT = SHINING_LIB_DIR + "/" + LIB_NAME
    if os.path.exists(OUTPUT + ".7z"):
        return
    
    print("Build OSL archive " + OUTPUT + ".7z for shining...")

    mkdir(OUTPUT + "/include")
    copy(CMAKE_INSTALL_PREFIX[0] + "/include/OSL", OUTPUT + "/include/OSL")

    mkdir(OUTPUT + "/shaders")
    copy(CMAKE_INSTALL_PREFIX[0] + "/shaders/stdosl.h", OUTPUT + "/shaders/stdosl.h")

    for i, build_type in enumerate(BUILD_TYPES):
        cmake_build_type = "Debug" if build_type == "Debug" else "RelWithDebInfo"
        mkdir(OUTPUT + "/lib/" + build_type)
        mkdir(OUTPUT + "/bin/" + build_type)
        lib_files = ["oslcomp.lib", "oslexec.lib"]
        for f in lib_files:
            copy(CMAKE_INSTALL_PREFIX[i] + "/lib/" + f, OUTPUT + "/lib/" + build_type + "/" + f)
        bin_files = ["oslc.exe", "oslcomp.dll", "oslexec.dll"]
        for f in bin_files:
            copy(CMAKE_INSTALL_PREFIX[i] + "/bin/" + f, OUTPUT + "/bin/" + build_type + "/" + f)
        copy(BUILD_DIR[i] + "/src/liboslcomp/" + cmake_build_type + "/oslcomp.pdb", OUTPUT + "/bin/" + build_type + "/oslcomp.pdb")
        copy(BUILD_DIR[i] + "/src/liboslexec/" + cmake_build_type + "/oslexec.pdb", OUTPUT + "/bin/" + build_type + "/oslexec.pdb")

    call7zip("a " + OUTPUT + ".7z " + OUTPUT)


def build_embree():
    LIB_NAME="embree_" + EMBREE_SUFFIX
    BUILD_DIR = [ ROOT_DIR + "/build-" + LIB_NAME + x for x in BUILD_DIR_SUFFIX ]
    GIT_TAG = "v" + EMBREE_VERSION
    CLONE_DIR = "repo-" + LIB_NAME
    INSTALL_PREFIX = [ ROOT_DIR + "/dist-" + LIB_NAME + "-" + build_type for build_type in BUILD_TYPES ]

    if not os.path.exists(CLONE_DIR):
        git("clone https://github.com/embree/embree.git " + CLONE_DIR)
        with pushd(CLONE_DIR):
            git("checkout " + GIT_TAG)

    for i, build_type in enumerate(BUILD_TYPES):
        cmake_build_type = build_type
        tbb_lib = "tbb.lib"
        tbb_malloc_lib = "tbbmalloc.lib"
        if not os.path.exists(BUILD_DIR[i]):
            mkdir(BUILD_DIR[i])
            with pushd(BUILD_DIR[i]):
                cmake(" -DCMAKE_BUILD_TYPE=" + cmake_build_type +
                    " -DCMAKE_INSTALL_PREFIX=" + INSTALL_PREFIX[i] +
                    " -DCMAKE_CXX_FLAGS_RELEASE=/Zi" +
                    " -DCMAKE_SHARED_LINKER_FLAGS_RELEASE=/DEBUG /OPT:REF /OPT:ICF" +
                    " -DEMBREE_TASKING_SYSTEM=TBB" +
                    " -DEMBREE_TBB_ROOT=" + SHINING_3RD_PARTY_DIR + "/tbb-2017.1" +
                    " -DTBB_INCLUDE_DIR=" + SHINING_3RD_PARTY_DIR + "/tbb-2017.1/include" +
                    " -DTBB_LIBRARY=" + SHINING_3RD_PARTY_DIR + "/tbb-2017.1/lib/intel64/vc" + VS_VERSIONS[VS_YEAR] + "/" + tbb_lib +
                    " -DTBB_LIBRARY_MALLOC=" + SHINING_3RD_PARTY_DIR + "/tbb-2017.1/lib/intel64/vc" + VS_VERSIONS[VS_YEAR] + "/" + tbb_malloc_lib +
                    " -DEMBREE_IGNORE_INVALID_RAYS=1" + 
                    " -DEMBREE_RAY_MASK=1" + 
                    " -DEMBREE_MAX_ISA=AVX"
                    " -DEMBREE_ISPC_SUPPORT=0" + 
                    " -DEMBREE_STATIC_LIB=0" + 
                    " -DEMBREE_TUTORIALS=0" + 
                    " " + ROOT_DIR + "/" + CLONE_DIR)
        with pushd(BUILD_DIR[i]):
            build("INSTALL.vcxproj", cmake_build_type)

    OUTPUT = SHINING_LIB_DIR + "/" + LIB_NAME
    if os.path.exists(OUTPUT + ".7z"):
        return
    
    print("Build embree archive " + OUTPUT + ".7z for shining...")

    copy(INSTALL_PREFIX[0] + "/include", OUTPUT + "/include")

    for i, build_type in enumerate(BUILD_TYPES):
        mkdir(OUTPUT + "/lib/" + build_type)
        lib_files = ["embree.lib", "embree.dll", "tbb.dll", "tbbmalloc.dll"]
        for f in lib_files:
            copy(INSTALL_PREFIX[i] + "/lib/" + f, OUTPUT + "/lib/" + build_type + "/" + f)
        copy(BUILD_DIR[i] + "/" + build_type + "/embree.pdb", OUTPUT + "/lib/" + build_type + "/embree.pdb")

    call7zip("a " + OUTPUT + ".7z " + OUTPUT)

def build_opensubdiv():
    LIB_NAME="OpenSubdiv_" + OPENSUBDIV_SUFFIX
    BUILD_DIR = [ ROOT_DIR + "/build-" + LIB_NAME + x for x in BUILD_DIR_SUFFIX ]
    GIT_TAG = "v" + OPENSUBDIV_VERSION
    CLONE_DIR = "repo-" + LIB_NAME
    INSTALL_PREFIX = [ os.path.join(ROOT_DIR, "dist-" + LIB_NAME + "-" + build_type) for build_type in BUILD_TYPES ]

    if not os.path.exists(CLONE_DIR):
        git("clone https://github.com/PixarAnimationStudios/OpenSubdiv " + CLONE_DIR)
        with pushd(CLONE_DIR):
            git("checkout " + GIT_TAG)

    for i, build_type in enumerate(BUILD_TYPES):
        cmake_build_type = build_type
        if not os.path.exists(BUILD_DIR[i]):
            mkdir(BUILD_DIR[i])
            with pushd(BUILD_DIR[i]):
                cmake(" -DCMAKE_BUILD_TYPE=" + cmake_build_type +
                    " -DCMAKE_INSTALL_PREFIX=" + INSTALL_PREFIX[i] +
                    " -DCMAKE_CXX_FLAGS_RELEASE=/Zi" +
                    " -DCMAKE_SHARED_LINKER_FLAGS_RELEASE=/DEBUG /OPT:REF /OPT:ICF" +
                    " -DNO_CLEW=1" +
                    " -DNO_CUDA=1" +
                    " -DNO_DOC=1" +
                    " -DNO_DX=1" +
                    " -DNO_EXAMPLES=1" +
                    " -DNO_GLTESTS=1" +
                    " -DNO_MAYA=1" +
                    " -DNO_OPENCL=1" +
                    " -DNO_OPENGL=1" +
                    " -DNO_PTEX=1" +
                    " -DNO_REGRESSION=1" +
                    " -DNO_TESTS=1" +
                    " -DNO_TUTORIALS=1" +
                    " -DTBB_INCLUDE_DIR=" + SHINING_3RD_PARTY_DIR + "/tbb-4.4.5/include" +
                    " -DTBB_tbb_LIBRARY=" + SHINING_3RD_PARTY_DIR + "/tbb-4.4.5/lib/intel64/vc" + VS_VERSIONS[VS_YEAR] + "/tbb.lib" +
                    " -DTBB_tbb_debug_LIBRARY=" + SHINING_3RD_PARTY_DIR + "/tbb-4.4.5/lib/intel64/vc" + VS_VERSIONS[VS_YEAR] + "/tbb_debug.lib" +
                    " -DTBB_tbb_preview_LIBRARY=" + SHINING_3RD_PARTY_DIR + "/tbb-4.4.5/lib/intel64/vc" + VS_VERSIONS[VS_YEAR] + "/tbb_preview.lib" +
                    " -DTBB_tbb_preview_debug_LIBRARY=" + SHINING_3RD_PARTY_DIR + "/tbb-4.4.5/lib/intel64/vc" + VS_VERSIONS[VS_YEAR] + "/tbb_preview_debug.lib" +
                    " -DTBB_tbbmalloc_LIBRARY=" + SHINING_3RD_PARTY_DIR + "/tbb-4.4.5/lib/intel64/vc" + VS_VERSIONS[VS_YEAR] + "/tbbmalloc.lib" +
                    " -DTBB_tbbmalloc_debug_LIBRARY=" + SHINING_3RD_PARTY_DIR + "/tbb-4.4.5/lib/intel64/vc" + VS_VERSIONS[VS_YEAR] + "/tbbmalloc_debug.lib" +
                    " -DTBB_tbbmalloc_proxy_LIBRARY=" + SHINING_3RD_PARTY_DIR + "/tbb-4.4.5/lib/intel64/vc" + VS_VERSIONS[VS_YEAR] + "/tbbmalloc_proxy.lib" +
                    " -DTBB_tbbmalloc_proxy_debug_LIBRARY=" + SHINING_3RD_PARTY_DIR + "/tbb-4.4.5/lib/intel64/vc" + VS_VERSIONS[VS_YEAR] + "/tbbmalloc_proxy_debug.lib" +
                    " " + ROOT_DIR + "/" + CLONE_DIR)
        with pushd(BUILD_DIR[i]):
            build("INSTALL.vcxproj", cmake_build_type)

    OUTPUT = SHINING_LIB_DIR + "/" + LIB_NAME
    if os.path.exists(OUTPUT + ".7z"):
        return
    
    print("Build opensubdiv archive " + OUTPUT + ".7z for shining...")

    copy(INSTALL_PREFIX[0] + "/include", OUTPUT + "/include")

    for i, build_type in enumerate(BUILD_TYPES):
        mkdir(OUTPUT + "/lib/" + build_type)
        copy(INSTALL_PREFIX[i] + "/lib/osdCPU.lib", OUTPUT + "/lib/" + build_type + "/osdCPU.lib")

    call7zip("a " + OUTPUT + ".7z " + OUTPUT)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description = 'Shining third party builder')
    parser.add_argument("--VS", \
        help = "Version for Visual Studio compilation. Default to 2015")
    parser.add_argument("--libs", help = "List of libs to build. If not specified, build all libs.", type = valid_list)

    args = parser.parse_args()

    if not args.VS:
        args.VS = DEFAULT_VS_VERSION

    if args.libs:
        libs = set(args.libs)
    else:
        libs = set(["precomp", "openexr", "llvm", "boost", "oiio", "osl", "embree", "opensubdiv"])

    VS_YEAR = args.VS
    VS_VERSION = VS_VERSIONS[VS_YEAR]
    BUILD_DIR_SUFFIX = [ "-vs-" + VS_YEAR + "-" + build_type for build_type in BUILD_TYPES ]
    CMAKE_GENERATOR = "Visual Studio " + VS_VERSION + " " + VS_YEAR + " Win64"

    SHINING_3RD_PARTY_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)), "../precompiled/ShiningVisual" + VS_YEAR)

    for install_prefix in CMAKE_INSTALL_PREFIX:
        mkdir(os.path.join(install_prefix, "include"))
        mkdir(os.path.join(install_prefix, "lib"))
        mkdir(os.path.join(install_prefix, "bin"))

    mkdir(SHINING_LIB_DIR)

    if "precomp" in libs:
        # Copy precompiled binaries to dist directories
        build_libjpeg()
        build_libtiff()
        build_libpng()
        build_zlib()

    if "openexr" in libs:
        build_openexr()
    if "llvm" in libs:
        build_llvm()
    if "boost" in libs:
        build_boost()
    if "oiio" in libs:
        build_oiio()
    if "osl" in libs:
        build_OSL()
    if "embree" in libs:
        build_embree()
    if "opensubdiv" in libs:
        build_opensubdiv()